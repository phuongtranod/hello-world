﻿using System;
using System.Text;
using Umbraco.Web.Trees;
using umbraco.cms.presentation.Trees;

namespace Admin.Trees
{
    [Tree("admin", "admin", "Admin")]
    public class AdminTree:BaseTree
    {
        public AdminTree(string application) : base(application)
        {
        }

        public override void RenderJS(ref StringBuilder Javascript)
        {
            Javascript.Append(
                    @"function openManageArticle(){
                        UmbClientMgr.contentFrame('/Admin/HomeSurface/ManageArticle');
                    }");
        }

        public override void Render(ref XmlTree tree)
        {
            XmlTreeNode xmlTreeNode = XmlTreeNode.Create(this);

            xmlTreeNode.Action = "javascript: openManageArticle();";
            xmlTreeNode.NodeID = "1";
            xmlTreeNode.Text = "Manage Article";
            xmlTreeNode.Icon = "folder.gif";
            xmlTreeNode.OpenIcon = "folder_o.gif";
            
            OnAfterNodeRender(ref tree, ref xmlTreeNode, EventArgs.Empty);
            if (xmlTreeNode!=null)
            {
                tree.Add(xmlTreeNode);
                OnAfterNodeRender(ref tree, ref xmlTreeNode, EventArgs.Empty);
            }

        }

        protected override void CreateRootNode(ref XmlTreeNode rootNode)
        {
            rootNode.Text = "Admin";
        }
    }
}
﻿using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace Admin.Controllers
{
    /// <summary>
    /// Home controller
    /// </summary>
    [PluginController("Admin")]
    public class HomeSurfaceController : SurfaceController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HomeSurfaceController"/> class.
        /// </summary>
        public HomeSurfaceController()
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeSurfaceController"/> class.
        /// </summary>
        /// <param name="umbracoContext"></param>
        public HomeSurfaceController(UmbracoContext umbracoContext) : base(umbracoContext)
        {

        }


        /// <summary>
        /// Manages the article.
        /// </summary>
        /// <returns></returns>
        public ActionResult ManageArticle()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }
    }
}

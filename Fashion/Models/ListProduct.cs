﻿using System.Collections.Generic;

namespace Fashion.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class ListProduct
    {
        /// <summary>
        /// Gets or sets the products.
        /// </summary>
        /// <value>
        /// The products.
        /// </value>
        public List<Product> Products { get; set; }

        /// <summary>
        /// Gets or sets the page information.
        /// </summary>
        /// <value>
        /// The page information.
        /// </value>
        public PageInformation PageInformation { get; set; }
    }
}
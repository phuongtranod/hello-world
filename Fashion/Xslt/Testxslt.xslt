<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [ <!ENTITY nbsp "&#x00A0;"> ]>
<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxml="urn:schemas-microsoft-com:xslt"
	xmlns:umbraco.library="urn:umbraco.library" xmlns:Examine="urn:Examine" 
	exclude-result-prefixes="msxml umbraco.library Examine ">


<xsl:output method="xml" omit-xml-declaration="yes"/>

<xsl:param name="currentPage"/>

<xsl:variable name="mediaCurrent" select="/macro/mediaCurrent"/>
<xsl:variable name="imageWidth" select="/macro/imageWidth"/>
<xsl:variable name="imageAlt" select="/macro/imageAlt"/>

<xsl:template match="/">

  <xsl:variable name="imgUrl" select="$mediaCurrent/node/data [@alias='umbracoFile']"/>
    <xsl:if test="$imgUrl != ''">        
        <img>
            <xsl:attribute name="src">
                <xsl:text>/umbraco/ImageGen.ashx?image=</xsl:text>
                <xsl:value-of select="$imgUrl"/>    
                <xsl:text>&amp;constrain=true</xsl:text>
                <xsl:text>&amp;width=</xsl:text>
                <xsl:value-of select="$imageWidth"/>
            </xsl:attribute>

            <xsl:attribute name="alt">
                <xsl:value-of select="$imageAlt"/>
            </xsl:attribute>
        </img>
        </xsl:if>

</xsl:template>

</xsl:stylesheet>
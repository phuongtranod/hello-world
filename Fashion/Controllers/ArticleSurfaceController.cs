﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Examine;
using Fashion.Models;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using umbraco;

namespace Fashion.Controllers
{
    public class ArticleSurfaceController : SurfaceController
    {
        private const int PageSize = 2;

        public ActionResult GetArticleByCategory(int id)
        {
            //var search = ExamineManager.Instance.SearchProviderCollection["ArticleSearcher"];
            //var searchCriteria = search.CreateSearchCriteria();
            //var query = searchCriteria.Field("titleArticle", "Jodie").Compile();
            //var result = search.Search("LP", true);

            //var a = result.Count();

            //var search = ExamineManager.Instance.SearchProviderCollection["WebsiteSearcher"];
            ////var searchCriteria = search.CreateSearchCriteria();
            ////var query = searchCriteria.Field("titleArticle", "Jodie").Compile();
            //var result = search.Search("LP", true);

            //var a = result.Count();


            var articles = uQuery.GetNodesByType("Article");


            return PartialView("_ArticleDetails", articles.Where(x => x.Parent.Id == id));
        }

        public ActionResult ListProduct(int page = 1, string sortType = "ASC")
        {
            var products = uQuery.GetNodesByType("Product");

            List<Product> listProduct = products.Select(product => new Product
                {
                    Desciption = product.GetProperty<string>("descriptionProduct"), 
                    Name = product.GetProperty<string>("nameProduct"), 
                    Image = product.GetProperty<string>("imageProduct"), 
                    Url = product.Url
                }).ToList();

            var pagingInformation = new PageInformation
                {
                    CurrentPage = 0,
                    PageSize = 56,
                    TotalRecord = listProduct.Count
                };

            var productWrap = new ListProduct
                {
                    Products = listProduct.OrderByDescending(x => x.Name).Skip((page - 1) * PageSize).Take(PageSize).ToList(), 
                    PageInformation = pagingInformation
                };

            return PartialView("AllProducts", productWrap);
        }

        [HttpPost]
        public ActionResult Search(string txtSearch)
        {
            var search = ExamineManager.Instance.SearchProviderCollection["ArticleSearcher"];
            var result = search.Search(txtSearch, true);

            List<Article> articles = new List<Article>();
            foreach (var searchResult in result)
            {
                articles.Add(new Article
                    {
                        ID = searchResult.Id,
                        Title = searchResult.Fields["titleArticle"]
                    });
            }

            return View("SearchResult", articles);
        }
    }
}